# moho-skeleton-tuner

## Установка

Для пользовательской установки в Moho, скачайте архив репозитория.

Для участия в разработке клонируйте репозиторий в отдельную папку, а в Moho создайте ярлыки для всех файлов. Это можно сделать таким bat-скриптом:

```
@echo off
setlocal enableDelayedExpansion
if "%~1"=="" (
    echo Please provide the source folder path as first argument.
    exit /b 1
)
if "%~2"=="" (
    echo Please provide the destination folder path as second argument.
    exit /b 1
)
set "source=%~1"
set "dest=%~2"
set "sourcefolder=%~dpn1"
echo %sourcefolder%
call :strlen len %sourcefolder%
echo %len%

for /r "%source%" %%F in (*) do (
    set "subdir=%%~dpF"
    set "subdir=!subdir:~%len%!"
    set "destsub=%dest%\Moho Pro\Scripts!subdir!"
    rem if not exist "!destsub!" md "!destsub!"
	if exist "!destsub!%%~nxF" del "!destsub!%%~nxF"
	echo mklink "!destsub!%%~nxF" "%%F"
    mklink "!destsub!%%~nxF" "%%F"
)
echo Done.
exit /b


:strlen
set "s=%~2"
set "len=0"
for /l %%i in (0,1,8191) do if not "!s:~%%i,1!"=="" set /a "len=%%i+1"
set "%~1=%len%"
exit /b
```
, где первый параметр -- папка с клоном репозитория, а второй -- Moho Content Folder.
Запускать скрипт нужно от имени администратора.

## План действий по разработке

1. **Режим работы инструмента для перемещения суставов**: брать за любой сустав, и перемещать этот сустав в любое место, не перемещая других суставов. Т.е. меняются длина и угол всех костей, составляющих сустав и позиция костей, которые в суставе начинаются (очевидно, это дочерние кости), а все остальные характеристики всех костей остаются прежними
2. **Режим работы инструмета для разбиения костей**: щелкать между суставами по телу кости и таким образом делить ее в этом месте на две кости, переименовывая соответствующим образом все кости, имевшие к этому отношение.
3. **кнопки в панели настроек, копирующие структуру** (т.е. размеры, позиции, углы, количество и имена костей) с левой стороны скелета на правую или с правой на левую.
