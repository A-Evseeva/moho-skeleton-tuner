-- **************************************************
-- Provide Moho with the name of this script object
-- **************************************************

ScriptName = "RS_SkeletonTuner"

-- **************************************************
-- General information about this script
-- **************************************************

RS_SkeletonTuner = {}

function RS_SkeletonTuner:Name()
	return 'Skeleton Tuner'
end

function RS_SkeletonTuner:Version()
	return '1.0'
end

function RS_SkeletonTuner:UILabel()
	return 'Skeleton Tuner'
end

function RS_SkeletonTuner:Creator()
	return 'Revival Studio'
end

function RS_SkeletonTuner:Description()
	return 'Move joints, split bones and keep symmetry at frame 0'
end

function RS_SkeletonTuner:ColorizeIcon()
	return true
end

-- **************************************************
-- Is Relevant / Is Enabled
-- **************************************************

function RS_SkeletonTuner:IsRelevant(moho)
	return true
end

function RS_SkeletonTuner:IsEnabled(moho)
	return moho:LayerAsBone(moho.layer)
end

-- **************************************************
-- Keyboard/Mouse Control
-- **************************************************

function RS_SkeletonTuner:NonDragMouseMove()
	return true
end

function RS_SkeletonTuner:OnMouseDown(moho, mouseEvent)
	
end

function RS_SkeletonTuner:OnMouseMoved(moho, mouseEvent)
	
end

function RS_SkeletonTuner:OnMouseUp(moho, mouseEvent)
	
end

function RS_SkeletonTuner:OnKeyDown(moho, keyEvent)
	
end

function RS_SkeletonTuner:OnKeyUp(moho, keyEvent)
	
end

function RS_SkeletonTuner:OnInputDeviceEvent(moho, deviceEvent)
	
end

function RS_SkeletonTuner:DrawMe(moho, view)
	local mesh = moho:DrawingMesh()
	if (mesh == nil) then
		return
	end

	local g = view:Graphics()
	local matrix = LM.Matrix:new_local()

	moho.drawingLayer:GetFullTransform(moho.frame, matrix, moho.document)
	g:Push()
	g:ApplyMatrix(matrix)


end

-- **************************************************
-- Tool Panel Layout
-- **************************************************

RS_SkeletonTuner.BUTTON_2LEFT = MOHO.MSG_BASE
RS_SkeletonTuner.BUTTON_2RIGHT = MOHO.MSG_BASE + 1

function RS_SkeletonTuner:DoLayout(moho, layout)
	if type(math.atan2) == "nil" then
		math.atan2 = math.atan
	end

	self.copy2leftButton = LM.GUI.Button('←|←', self.BUTTON_2LEFT)
	layout:AddChild(self.copy2leftButton, LM.GUI.ALIGN_LEFT, 0)
	self.copy2leftButton:SetToolTip('Copy structure from right to left')

	self.copy2rightButton = LM.GUI.Button('→|→', self.BUTTON_2RIGHT)
	layout:AddChild(self.copy2rightButton, LM.GUI.ALIGN_LEFT, 0)
	self.copy2rightButton:SetToolTip('Copy structure from right to left')
end

function RS_SkeletonTuner:HandleMessage(moho, view, msg)
	if msg == self.BUTTON_2LEFT then
		self:MirrorBoneTransforms(moho, true)
	elseif msg == self.BUTTON_2RIGHT then
		self:MirrorBoneTransforms(moho, false)
	else
		
	end
end

function RS_SkeletonTuner:MirrorBoneTransforms(moho, r2l)
	local skel = moho:Skeleton()
	--TODO: apply to selected bones, or to all bones if nothing selected
	--now can work with a single selected bone only
	local curBone = skel:Bone(skel:SelectedBoneID())
	self:MirrorSingleBoneTransform(moho, skel, curBone)
end

function RS_SkeletonTuner:MirrorSingleBoneTransform(moho, skel, sourceBone)

	--find target bone by name
	local name = sourceBone:Name()
	local lastChar = name:sub(-1)
	local newChar
	if lastChar == 'L' then newChar = 'R'
	elseif lastChar == 'R' then newChar = 'L'
	else return end
	targetName = name:sub(1,-2)..newChar
	targetBone = skel:BoneByName(targetName)
	if not targetBone then return end
	
	--get global mirrored position and angle
	local zeroVec = LM.Vector2:new_local()
	local normVec = LM.Vector2:new_local()
	normVec:Set(1,0)
	sourceBone.fRestMatrix:Transform(zeroVec)
	sourceBone.fRestMatrix:Transform(normVec)
	normVec = normVec - zeroVec
	normVec.x = -normVec.x
	zeroVec.x = -zeroVec.x
	normVec = zeroVec + normVec
	
	--get local position and angle for target bone
	local invMatrix = LM.Matrix:new_local()
	if targetBone.fParent > -1 then
		invMatrix:Set(skel:Bone(targetBone.fParent).fRestMatrix)
	end
	invMatrix:Invert()
	invMatrix:Transform(zeroVec)
	targetBone.fAnimPos:SetValue(0, zeroVec)
	invMatrix:Transform(normVec)
	normVec = normVec - zeroVec
	targetBone.fAnimAngle:SetValue(0, math.atan(normVec.y, normVec.x)) 
	
	targetBone.fLength = sourceBone.fLength
	
end
